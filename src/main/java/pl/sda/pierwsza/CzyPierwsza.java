package pl.sda.pierwsza;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class CzyPierwsza implements Runnable {
    private CountDownLatch latch;
    private long candidate;
    private AtomicInteger atomicInteger;

    public CzyPierwsza(long candidate, CountDownLatch latch, AtomicInteger atomicInteger) {
        this.candidate = candidate;
        this.latch = latch;
        this.atomicInteger = atomicInteger;
    }

    @Override
    public void run() {
        double sqrt = Math.sqrt(candidate);

        if (candidate <= 1) {
            latch.countDown();
            return;
        }

        for (int i = 2; i < candidate; i++) {
            if (candidate % i == 0) {
                latch.countDown();
                return;
            }
        }


        System.out.println(candidate + " is prime");
        System.out.println("We have: " + atomicInteger.incrementAndGet() + " primes!");
        latch.countDown();

    }
}

