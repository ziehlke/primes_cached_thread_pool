import pl.sda.pierwsza.CzyPierwsza;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {


    public static void main(String[] args) throws FileNotFoundException, InterruptedException {

        CountDownLatch latch = new CountDownLatch(5001);
        final long start = System.currentTimeMillis();

        Scanner scanner = new Scanner(new File("src/main/resources/numbers.txt"));

        AtomicInteger atomicInteger = new AtomicInteger(0);
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        while (scanner.hasNextLong()) {
            cachedThreadPool.execute(new CzyPierwsza(scanner.nextLong(), latch, atomicInteger));
        }

        cachedThreadPool.shutdown();

        latch.countDown();
        latch.await();

        System.out.println();
        System.out.println("--------------------------------------------------------------------------------------");
        System.out.println("Found: " + atomicInteger.get() + " prime numbers.");
        System.out.println("computation took: " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start) + " seconds.");
    }
}
